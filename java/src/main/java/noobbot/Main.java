package noobbot;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

import noobbot.model.AIInfo;
import noobbot.model.Car;
import noobbot.model.CarPosition;
import noobbot.model.CarSpecification;
import noobbot.model.LapResult;
import noobbot.model.Piece;
import noobbot.model.PiecePosition;
import noobbot.model.PieceType;
import noobbot.model.Race;
import noobbot.model.RacePart;
import noobbot.model.ResponseCarPositions;
import noobbot.model.ResponseCrashed;
import noobbot.model.ResponseGameEnd;
import noobbot.model.ResponseGameInit;
import noobbot.model.ResponseLapFinished;
import noobbot.model.ResponseSpawned;
import noobbot.model.ResponseTurboAvailable;
import noobbot.model.ResponseYourCar;
import noobbot.model.SwitchLaneType;
import noobbot.model.Track;
import noobbot.model.msg.Join;
import noobbot.model.msg.JoinRace;
import noobbot.model.msg.MsgWrapper;
import noobbot.model.msg.Ping;
import noobbot.model.msg.SendMsg;
import noobbot.model.msg.SwitchLane;
import noobbot.model.msg.Throttle;
import noobbot.model.msg.TurboRequest;

public class Main {

    // testserver.helloworldopen.com:8091 as Albatroz/1IzoD7N+Rsn7gA

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        botName = args[2];
        botKey = args[3];

        if (args.length > 4) {

            trackId = args[4];
            if (args.length > 5) {
                numCars = Integer.parseInt(args[5]);
            }

        }

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        try {
            final Socket socket = new Socket(host, port);
            final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
            final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
            new Main(reader, writer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    Race race;
    public static Track track;

    static String botName;
    static String botKey;

    static String trackId;
    static int numCars = -1;

    boolean log = true;
    boolean wroteTrack = false;

    private void logPrint(String msg) {
        if (log) {
            System.out.print(msg);
        }
    }

    private void logPrintln(String msg) {
        if (log) {
            System.out.println(msg);
        }
    }


    public Main(final BufferedReader reader, final PrintWriter writer) throws IOException {

        System.out.println("Main()");


        try {

            this.writer = writer;
            String line = null;

            if (trackId == null) {
                send(new Join(botName, botKey));
            } else {
                if (numCars > 0) {
                    send(new JoinRace(botName, botKey, trackId, numCars));
                } else {
                    send(new JoinRace(botName, botKey, trackId));
                }
            }

            long prevTick = -1;
            int prevPieceIndex = -1;
            double prevDistance = -1;

            double prevThrottle = -2;
            double throttle = -2;

            double prevCarAngle = -1;
            double prevCarAngleABS = -1;
            double prevCarAngleDeltaABS = -1;

            int changedLane = -1;

            int curLane = -1;
            int nextLane = -1;

            double laneDelta = 0;

            int lastCrashPieceIndex = -1;

            double speed = 0;
            double prevSpeed = 0;

            double pieceMaxSpeed = 0;

            long pieceStartTick = 0;

            boolean turboBoost = false;
            long turboEndTick = 0;

            boolean crashed = false;

            Piece prevPiece = null;
            Piece thisPiece = null;
            Piece nextPiece = null;
            Piece secondPiece = null;

            Map<String, CarPosition> otherCarPositions = new HashMap<String, CarPosition>();
            Car car = null;
            String gameId;

            double avgAccelleration = 0;
            double avgBraking = 0;

            while ((line = reader.readLine()) != null) {
                //System.out.println(line);

                final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);

                logPrint(".");


                if (msgFromServer.msgType.equals("carPositions")) {

                    ResponseCarPositions rcp = gson.fromJson(line, ResponseCarPositions.class);

                    for (CarPosition cp : rcp.getCarPositions()) {

                        if (!cp.getCar().equals(car)) {

                            otherCarPositions.put(cp.getCar().getName(), cp);

                        } else {

                            //logPrint(cp.getCar().toString() + " ");

                            int thisPieceIndex = cp.getPiecePosition().getPieceIndex();
                            double inPieceDistance = cp.getPiecePosition().getInPieceDistance();

                            prevPiece = track.getPiece(thisPieceIndex - 1);
                            thisPiece = track.getPiece(thisPieceIndex);
                            nextPiece = track.getPiece(thisPieceIndex + 1);
                            secondPiece = track.getPiece(thisPieceIndex + 2);

                            curLane = cp.getPiecePosition().getLaneSwitch().getStartLaneIndex();
                            if (thisPiece.isLaneSwitch() || nextLane < 0) {
                                nextLane = cp.getPiecePosition().getLaneSwitch().getEndLaneIndex();
                            }

                            laneDelta = track.getLanes().get(curLane).getDistanceFromCenter();

                            if (rcp.getGameTick() > 0) {

                                double delta = 0;
                                if (prevPieceIndex != thisPieceIndex) {

                                    if (prevPiece.isLaneSwitch() && changedLane == prevPieceIndex) {
                                        delta += 2.05;
                                    }

                                    delta += (prevPiece.getLength(laneDelta) - prevDistance + inPieceDistance);
                                } else {
                                    delta += inPieceDistance - prevDistance;
                                }

                                speed = delta / (rcp.getGameTick() - prevTick);

                                if (turboBoost && turboEndTick < rcp.getGameTick()) {
                                    turboBoost = false;
                                }

                            }

                            if (prevPieceIndex != thisPieceIndex) {

                                prevPiece.getAiInfo().setMaxSpeed(curLane, Math.max(prevPiece.getAiInfo().getMaxSpeed(curLane), pieceMaxSpeed));
                                pieceMaxSpeed = 0;
                                pieceStartTick = rcp.getGameTick();

                            }

                            pieceMaxSpeed = Math.max(pieceMaxSpeed, speed);

                            if (!crashed) {
                                double delta = 0;
                                if (prevThrottle == 1 && throttle == 1) {
                                    if (speed > prevSpeed) {
                                        delta = speed - prevSpeed;
                                        if (delta > 0) {
                                            if (track.getAvgAccelleration() == 0) {
                                                track.setAvgAccelleration(delta);
                                            } else {
                                                track.setAvgAccelleration((track.getAvgAccelleration() + delta) / 2);
                                            }

                                            if (track.getAvgBraking() == 0) {
                                                track.setAvgBraking(track.getAvgAccelleration());
                                            }
                                        }
                                    }

                                } else if (prevThrottle == 0 && throttle == 0) {
                                    if (speed < prevSpeed) {
                                        delta = prevSpeed - speed;
                                        if (delta > 0) {
                                            if (track.getAvgBraking() == 0) {
                                                track.setAvgBraking(delta);
                                            } else {
                                                track.setAvgBraking(((track.getAvgBraking() + delta) / 2) % 1);
                                            }
                                        }
                                    }
                                }
                            }
                            prevThrottle = throttle;

                            logPrint(String.format("Lap: %1d Piece: %2d (%d->%d) %s [%5.2f] @%6.2f ... %4.2f (%3.2f %3.2f)", cp.getPiecePosition().getLap(), cp.getPiecePosition().getPieceIndex(), curLane, nextLane, thisPiece.toString(laneDelta), cp.getPiecePosition().getInPieceDistance(), cp.getAngle(), speed, track.getAvgAccelleration(), track.getAvgBraking()) + " ... ");


                            double carAngleABS = Math.abs(cp.getAngle());

                            if (prevCarAngleABS < 0) {
                                prevCarAngleABS = carAngleABS;
                                prevCarAngle = cp.getAngle();
                                prevCarAngleDeltaABS = 0;
                            }

                            double carAngleDeltaABS = Math.abs(carAngleABS - prevCarAngleABS);

                            throttle = -1.0;

                            AIInfo info = thisPiece.getAiInfo();
                            int crashCount = info.getCrashCount(curLane);

                            AIInfo nextInfo = nextPiece.getAiInfo();
                            int nextCrashCount = nextInfo.getCrashCount(nextLane);

                            AIInfo secondInfo = secondPiece.getAiInfo();
                            int secondCrashCount = secondInfo.getCrashCount(nextLane);


                            int angleFactorThis = 7;
                            int angleFactorNext = 6;
                            int angleFactorSecond = 5;


                            // switcher
                            if (nextPiece.isLaneSwitch() && thisPieceIndex != changedLane) {

                                double[] ticks = track.getDistanceUntilNextSwitch(thisPieceIndex + 2); //track.getTicksUntilNextSwitch(thisPieceIndex);

                                logPrint(" SW: ");
                                for (int i = 0; i < ticks.length; i++) {
                                    logPrint(String.format("%6.2f ", ticks[i]));
                                }

                                int nextLaneCandidate = curLane;

                                double maxDistance = Integer.MAX_VALUE;

                                for (int i = 0; i < ticks.length; i++) {
                                    if (ticks[i] < maxDistance || (ticks[i] == maxDistance && i == curLane)) {
                                        nextLaneCandidate = i;
                                        maxDistance = ticks[i];
                                    }
                                }

                                int nextSwitchPiece = track.getNextSwitch(thisPieceIndex + 2);
                                int cntLeft = 0;
                                int cntRight = 0;

                                for (int i = thisPieceIndex ; i <= nextSwitchPiece;  i++) {
                                    Piece p = track.getPiece(i);
                                    if (!p.getType().equals(PieceType.straight) ) {
                                        if (p.getAngle() == 45 && p.getRadius() <= 50) {
                                            if (p.getType().equals(PieceType.curveLeft)) {
                                                cntLeft++;
                                            } else {
                                                cntRight++;
                                            }
                                        }
                                    }
                                }
                                logPrint(String.format(" %d<->%d ",cntLeft,cntRight));
                                if (cntLeft > 3) {
                                    logPrint(" <SHARP LEFT> ");
                                    nextLaneCandidate = track.getLanes().size() - 1;
                                } else if (cntRight > 3) {
                                    logPrint(" <SHARP RIGHT> ");
                                    nextLaneCandidate = 0;
                                }



                                if (maxDistance == Integer.MAX_VALUE) {

                                    PieceType curveType = track.getCurveTypeUntilNextSwitch(thisPieceIndex + 1);
                                    switch (curveType) {
                                        case curveLeft:
                                            if (curLane > 0) {
                                                logPrint(" <LEFT(c)> ");
                                                send(new SwitchLane(SwitchLaneType.Left));
                                                throttle = 10;
                                                nextLane = curLane - 1;
                                            }
                                            break;
                                        case curveRight:
                                            if (curLane > track.getLanes().size() - 1) {
                                                logPrint(" <RIGHT(c)> ");
                                                send(new SwitchLane(SwitchLaneType.Right));
                                                throttle = 10;
                                                nextLane = curLane + 1;
                                            }
                                            break;
                                        case straight:
                                            logPrint(" <STAY(c)> ");
                                    }

                                } else {

                                    if (nextLaneCandidate < curLane) {

                                        logPrint(" <LEFT> ");
                                        send(new SwitchLane(SwitchLaneType.Left));
                                        throttle = 10;
                                        nextLane = curLane - 1;

                                    } else if (nextLaneCandidate > curLane) {

                                        logPrint(" <RIGHT> ");
                                        send(new SwitchLane(SwitchLaneType.Right));
                                        throttle = 10;
                                        nextLane = curLane + 1;

                                    } else {

                                        logPrint(" <STAY> ");

                                    }

                                }
                                changedLane = thisPieceIndex;

                            }

                            boolean driftingAlert = false;
                            boolean drifting = false;

                            //if (throttle < 0) {
                                // drift detector
                                if (carAngleABS > 45) {
                                    if (prevCarAngleABS >= carAngleABS) {
                                        drifting = true;
                                    } else {
                                        driftingAlert = true;
                                    }
                                } else if ((carAngleABS > prevCarAngleABS && carAngleABS - prevCarAngleABS > 6)) {
                                    driftingAlert = true;
                                } else if ((carAngleABS > prevCarAngleABS && carAngleABS - prevCarAngleABS > 4)) {
                                    drifting = true;
                                }
                            //}

                            // look for car to bump
                            if (car.hasTurbo() && !turboBoost && !driftingAlert && throttle < 0 &&  otherCarPositions.size() > 0) {

                                for (CarPosition ocp : otherCarPositions.values()) {

                                    PiecePosition opp = ocp.getPiecePosition();

                                    if (car.hasTurbo() && !turboBoost &&
                                            ((opp.getPieceIndex() == thisPieceIndex + 1 &&
                                                opp.getLaneSwitch().getEndLaneIndex() == curLane) ||
                                                (opp.getPieceIndex() == thisPieceIndex &&
                                                        opp.getLaneSwitch().getEndLaneIndex() == curLane &&
                                                        opp.getInPieceDistance() > inPieceDistance
                                                ))) {

                                            turboBoost = true;
                                            turboEndTick = rcp.getGameTick() + car.getTurbo().getDurationTicks();

                                            send(new TurboRequest("jihaaaaa"));
                                            logPrint(" <BUMP TURBO> ");
                                            car.setTurbo(null);
                                            throttle = 10;


                                    }

                                }

                            }


                            if (throttle < 0 && track.getAvgBraking() > 0) {

                                double nextCrashDistance = 0;


                                int useLane ;

                                if (!thisPiece.isLaneSwitch() && curLane != nextLane) {
                                    useLane = curLane;
                                } else {
                                    useLane = nextLane;
                                }

                                Piece crashPiece = track.getNextCrashPiece(thisPieceIndex, useLane);
                                double crashSpeedNextLane = crashPiece.getAiInfo().getMinCrashSpeed(useLane);

                                logPrint(String.format(" NC: %d [%3.2f] ", crashPiece.getIndex(), crashSpeedNextLane));

                                if (thisPieceIndex > crashPiece.getIndex() && race.getRaceSession().getRacePart().equals(RacePart.race) &&
                                        cp.getPiecePosition().getLap() == race.getRaceSession().getLaps() - 1) {

                                    if (car.hasTurbo() && track.isStraightLane(thisPieceIndex)) {
                                        send(new TurboRequest("jihaaaaa"));
                                        logPrint(" <SPRINT TURBO> ");
                                        car.setTurbo(null);
                                        throttle = 10;
                                    } else {
                                        logPrint(" <SPRINT> ");
                                        throttle = 1;
                                    }

                                } else {

                                    Piece nextCrashPiece = track.getPiece(crashPiece.getIndex() + 1);
                                    if (nextCrashPiece.getAiInfo().isCrashPiece(useLane)) {
                                        double nextCrashSpeed = nextCrashPiece.getAiInfo().getMinCrashSpeed(useLane);

                                        if (nextCrashSpeed < crashSpeedNextLane) {
                                            logPrint(String.format(" 2C: %d [%3.2f] ", nextCrashPiece.getIndex(), nextCrashSpeed));
                                            crashSpeedNextLane = nextCrashSpeed;
                                        }
                                    }


                                    if (speed > crashSpeedNextLane) {

                                        logPrint(String.format(" %4.2f > %4.2f ", speed, crashSpeedNextLane));

                                        if (crashPiece.getIndex() != thisPieceIndex) {

                                            for (int i = (thisPieceIndex + 1); i != crashPiece.getIndex(); i = (i + 1) % track.getPieces().size()) {
                                                nextCrashDistance += track.getPiece(i).getLength(useLane);
                                            }
                                            nextCrashDistance += Math.abs(thisPiece.getLength(useLane) - cp.getPiecePosition().getInPieceDistance());

                                            if (nextCrashDistance >= 0) {

                                                double brakeDistance = 0;
                                                double curSpeed = speed;
                                                while (curSpeed > crashSpeedNextLane) {
                                                    brakeDistance += curSpeed;
                                                    curSpeed -= track.getAvgBraking();
                                                }

                                                logPrint(String.format(" %3.2f -> %3.2f in: %4.2f {%3.2f} ", speed, crashSpeedNextLane, brakeDistance, nextCrashDistance));

                                                if (brakeDistance >= (nextCrashDistance - 25)) {

                                                    logPrint(" <BRAKE> ");
                                                    throttle = 0;

                                                }
                                            }

                                        } else {

                                            // this is crash piece
                                            if (crashSpeedNextLane < 100) {

                                                logPrint(" <CRASHPIECE> ");

                                                if (speed > crashSpeedNextLane) {

                                                    logPrint(String.format(" %4.2f > %4.2f ", speed , info.getMinCrashSpeed(useLane)));
                                                    logPrint(" <BRAKE> ");
                                                    throttle = 0;
                                                }
                                            } else {

                                                logPrint(" <LANE CRASHFREE> ");
                                                logPrint(" <PETROL> ");
                                                throttle = 1;
                                            }

                                        }
                                    }
                                }
                            }

/*
                            if (throttle < 0) {

                                if (secondCrashCount > 0) {

                                    logPrint("2: " + secondInfo.getLaneInfo(nextLane));
                                    if (secondInfo.getMinCrashSpeed(nextLane) > 0 && secondInfo.getMinCrashSpeed(nextLane) < speed) {
                                        logPrint(String.format(" 2: %4.2f < %4.2f ", secondInfo.getMinCrashSpeed(nextLane), speed));
                                        throttle = 0;
                                    } else if (secondInfo.getMinCrashAngle(nextLane) > 45 && secondInfo.getMinCrashAngle(nextLane) < angleFactorSecond * carAngleDeltaABS + carAngleABS) {
                                        logPrint(String.format(" 2: %4.2f < %4.2f + %4.2f ", secondInfo.getMinCrashAngle(nextLane), angleFactorSecond * carAngleDeltaABS, carAngleABS));
                                        throttle = 0.5;
                                    }
                                }
                            }
*/


                            if (throttle < 0) {

                                if (nextCrashCount > 0) {

                                    logPrint("N: " + nextInfo.getLaneInfo(nextLane));

                                    if (nextInfo.getMinCrashAngle(nextLane) > 45 && nextInfo.getMinCrashAngle(nextLane) < angleFactorNext * carAngleDeltaABS + carAngleABS) {

                                        logPrint(String.format(" N: %4.2f < %4.2f + %4.2f ", nextInfo.getMinCrashAngle(nextLane), angleFactorNext * carAngleDeltaABS, carAngleABS));
                                        throttle = 0;

/*
                                    } else if ((nextInfo.getCrashCount(nextLane) > 0 && nextInfo.getMinCrashSpeed(nextLane) < speed)) {

                                        logPrint(String.format(" N: %4.2f < %4.2f ", nextInfo.getMinCrashSpeed(nextLane), speed));
                                        throttle = 3.33;

                                    } else if (nextInfo.getMaxSpeed(nextLane) > 4 && nextInfo.getMaxSpeed(nextLane) < speed) {

                                        logPrint(String.format(" N: %4.2f > %4.2f ", nextInfo.getMaxSpeed(nextLane), speed));
                                        throttle = 0;
*/
                                    }
                                }
                            }

/*
                            if (throttle < 0) {

                                if (crashCount > 0) {

                                    logPrint("T: " + info.getLaneInfo(curLane));

                                    if ( info.getMinCrashAngle(curLane) > 45 && info.getMinCrashAngle(curLane) < angleFactorThis * carAngleDeltaABS + carAngleABS) {
                                        logPrint(String.format(" T: %4.2f < %4.2f + %4.2f ", info.getMinCrashAngle(curLane), angleFactorThis * carAngleDeltaABS, carAngleABS));
                                        throttle = 0;
                                    }
                                }
                            }
*/


/*
                            if (throttle < 0) {
                                if (nextCrashCount == 0 && nextInfo.getMaxSpeed(curLane) > 0 && nextInfo.getMaxSpeed(curLane) >= speed) {
                                    logPrint(String.format(" N: %4.2f >= %4.2f ", nextInfo.getMaxSpeed(curLane), speed));
                                    throttle = 1;
                                }
                            }
*/

/*
                            if (throttle < 0) {
                                if (crashCount == 0 && info.getMaxSpeed(curLane) > 0 && info.getMaxSpeed(curLane) >= speed) {
                                    logPrint(String.format(" T: %4.2f >= %4.2f ", info.getMaxSpeed(curLane), speed));
                                    throttle = 1;

                                    if (car.hasTurbo() && track.isStraightLane(thisPieceIndex)) {

                                        send(new TurboRequest("jihaaaaa"));

                                        turboBoost = true;
                                        turboEndTick = rcp.getGameTick() + car.getTurbo().getDurationTicks();

                                        logPrint(" <TURBO> ");
                                        car.setTurbo(null);
                                        throttle = 10;
                                    }
                                }
                            }
*/


                            if (throttle < 0) {


                                if (track.isStraightLane(thisPieceIndex)) {

                                    logPrint(" SS ");
                                    throttle = 1.0;

                                    if (car.hasTurbo()) {

                                        turboBoost = true;
                                        turboEndTick = rcp.getGameTick() + car.getTurbo().getDurationTicks();

                                        send(new TurboRequest("jihaaaaa", rcp.getGameTick()));
                                        logPrint(" <TURBO> ");
                                        car.setTurbo(null);
                                        throttle = 10;
                                    }


                                } else if (track.isStraightLane(thisPieceIndex + 1)) {

                                    throttle = 1.0;
                                    logPrint(" ?S ");


                                    if (track.inCurve(thisPieceIndex)) {
                                        logPrint(" ?.6 ");
                                        throttle *= 0.6;
                                    }


                                    if (inPieceDistance > thisPiece.getLength(curLane) * .6 && car.hasTurbo()) {

                                        turboBoost = true;
                                        turboEndTick = rcp.getGameTick() + car.getTurbo().getDurationTicks();

                                        send(new TurboRequest("jihaaaaa", rcp.getGameTick()));
                                        logPrint(" <TURBO> ");
                                        car.setTurbo(null);
                                        throttle = 10;
                                    }

                                }

                                if (throttle < 0) {
                                    logPrint(" 1 ");
                                    throttle = 1;
                                }

                            }


                            if (0 <= throttle && throttle <= 1.0) {

                                if (throttle > 0) {

                                    double divider = 1.0;
                                    int cntSharpLeft = 0;
                                    int cntSharpRight = 0;
                                    int cntLeft = 0;
                                    int cntRight = 0;

                                    for (int i = 0 ; i < 4;  i++) {
                                        Piece p = track.getPiece(thisPieceIndex + i);
                                        if (!p.getType().equals(PieceType.straight)) {
                                            if (p.getType().equals(PieceType.curveLeft)) {
                                                cntLeft++;
                                                if (i < 3 && p.getLength() < 50) {
                                                    cntSharpLeft++;
                                                }
                                            } else {
                                                cntRight++;
                                                if (i < 3 && p.getLength() < 50) {
                                                    cntSharpRight++;
                                                }
                                            }
                                        }
                                    }
                                    if (cntLeft > 3 || cntRight > 3) {
                                        logPrint(" <LONG CURVE> ");
                                        divider *= 1.5;
                                    }
                                    if (cntSharpLeft + cntSharpRight > 2) {
                                        logPrint(" <CURVE ALERT> ");
                                        divider *= 1.5;
                                    }

                                    if (driftingAlert) {
                                        logPrint(" <DRIFT ALERT> ");
                                        divider *= 10;
                                    } else if (drifting) {
                                        logPrint(" <DRIFT> ");
                                        divider *= 1.5;
                                    }


                                    if (turboBoost && (nextPiece.getAiInfo().getMaxSpeed(curLane) < speed ||
                                            secondPiece.getAiInfo().getMaxSpeed(curLane) < speed ||
                                            track.getPiece(thisPieceIndex + 3).getAiInfo().getMaxSpeed(curLane) < speed)) {
                                        logPrint(" <TURBO CRASH ALERT> ");
                                        divider = 2;
                                    }


                                    if (divider > 1.0) {
                                        throttle /= divider;
                                    }
                                }


                                if (log) {
                                    int count = (int) (throttle * 10);

                                    double half = (throttle * 10) % 1;

                                    for (int i = 0; i < count; i++) {
                                        logPrint("[]");
                                    }
                                    if (half >= .5) {
                                        logPrint("[");
                                    }

                                    logPrintln("");
                                }

                                send(new Throttle(throttle, rcp.getGameTick()));

                            } else {
                                logPrintln("");
                                send(new Ping());
                            }


                            prevCarAngleABS = carAngleABS;
                            prevCarAngle = cp.getAngle();
                            prevPieceIndex = thisPieceIndex;
                            prevDistance = inPieceDistance;
                            prevTick = rcp.getGameTick();
                            prevSpeed = speed;
                            crashed = false;

                        }
                    }

                } else if (msgFromServer.msgType.equals("crash")) {


                    ResponseCrashed responseCrashed = gson.fromJson(line, ResponseCrashed.class);

                    if (thisPiece != null && responseCrashed.getCar().equals(car)) {

                        System.out.println("Crashed");

                        crashed = true;
                        log = false;
                        lastCrashPieceIndex = prevPieceIndex;

                        AIInfo info = thisPiece.getAiInfo();
                        info.addCrashOnLane(curLane);

                        double curCrashAngle = info.getMinCrashAngle(curLane);
                        if (curCrashAngle < 90) {
                            curCrashAngle = (curCrashAngle + prevCarAngleABS)/2;
                        } else {
                            curCrashAngle = prevCarAngleABS;
                        }
                        info.setMinCrashAngle(curLane, curCrashAngle);

                        double curCrashSpeed = info.getMinCrashSpeed(curLane);
                        if (curCrashSpeed < Integer.MAX_VALUE) {
                            curCrashSpeed = (curCrashSpeed + speed)/2;
                        } else {
                            curCrashSpeed = speed;
                        }
                        info.setMinCrashSpeed(curLane, curCrashSpeed);

                        double curCrashDistance = info.getAvgCrashDistance(curLane);
                        if (curCrashDistance > 0) {
                            curCrashDistance = (curCrashDistance + prevDistance)/2;
                        } else {
                            curCrashDistance = prevDistance;
                        }
                        info.setAvgCrashDistance(curLane, curCrashDistance);

                    }
                    send(new Ping());

                } else if (msgFromServer.msgType.equals("spawn")) {
                    ResponseSpawned responseSpawned = gson.fromJson(line, ResponseSpawned.class);
                    if (responseSpawned.getCar().equals(car)) {
                        System.out.println("Spawned");
                        log = true;
                    }
                    send(new Ping());
                } else if (msgFromServer.msgType.equals("join")) {
                    System.out.println("Joined");
                    send(new Ping());

                } else if (msgFromServer.msgType.equals("turboAvailable")) {
                    System.out.println("Got Turbo: " + car);
                    ResponseTurboAvailable turboAvailable = gson.fromJson(line, ResponseTurboAvailable.class);
                    car.setTurbo(turboAvailable.getTurbo());
                    send(new Ping());

                } else if (msgFromServer.msgType.equals("yourCar")) {


                    ResponseYourCar responseYourCar = gson.fromJson(line, ResponseYourCar.class);

                    car = responseYourCar.getCar();
                    gameId = responseYourCar.getGameId();

                    System.out.println("Got Car: " + car);
                    send(new Ping());

                } else if (msgFromServer.msgType.equals("gameInit")) {

                    System.out.println("Game init");

                    ResponseGameInit gameInit = gson.fromJson(line, ResponseGameInit.class);
                    race = gameInit.getGameData().getRace();
                    track = gameInit.getGameData().getRace().getTrack();

                    for (CarSpecification carSpecification : gameInit.getGameData().getRace().getCarSpecifications()) {

                        if (!carSpecification.getCar().equals(car)) {

                            CarPosition cp = new CarPosition();
                            cp.setCar(carSpecification.getCar());
                            otherCarPositions.put(carSpecification.getCar().getName(), cp);

                        }

                    }
                    send(new Ping());

                    readTrackData(track);

                    System.out.println(track.toString());


                } else if (msgFromServer.msgType.equals("lapFinished")) {

                    ResponseLapFinished responseLapFinished = gson.fromJson(line, ResponseLapFinished.class);
                    if (responseLapFinished.getLap().getCar().equals(car)) {
                        System.out.println("Lap Finished");
                        System.out.println("Lap Time: " + responseLapFinished.getLap().getLapTime().getMillis());
                    }
                    send(new Ping());

                } else if (msgFromServer.msgType.equals("gameEnd")) {

                    System.out.println("Game end");
                    ResponseGameEnd responseGameEnd = gson.fromJson(line, ResponseGameEnd.class);

                    System.out.println("Results:");
                    for (LapResult lr : responseGameEnd.getGameResults().getBestLaps()) {
                        System.out.println(" CAR: " + lr.getCar() + " : " + (double) (lr.getResult().getMillis()) / 1000);
                    }

                    writeTrackData(track);
                    send(new Ping());

                } else if (msgFromServer.msgType.equals("gameStart")) {

                    System.out.println("Game start");
                    send(new Ping());

                } else if (msgFromServer.msgType.equals("tournamentEnd")) {

                    System.out.println("Tournament End");
                    send(new Ping());

                } else {
                    send(new Ping());
                }
            }


            System.out.println("GOT: " + ((line == null) ? "null" : line.getBytes()));
            System.out.println("reader.ready(): " + reader.ready());


        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            if (!wroteTrack) {
                writeTrackData(track);
            }

        }

    }


    private void readTrackData(Track track) {

        if (track == null) {
            logPrintln("TRACK IS NULL (read)...");
        }
        for (int i = 0; i < track.getPieces().size(); i++) {
            track.getPiece(i).setIndex(i);
        }

        File file = new File(track.getId() + ".json");

        if (file.exists()) {

            BufferedReader bf = null;

            try {

                FileReader fr = new FileReader(file);
                StringBuilder sb = new StringBuilder();
                bf = new BufferedReader(new FileReader(file));
                String text = null;
                while ((text = bf.readLine()) != null) {
                    sb.append(text);
                }
                final Track trackAI = gson.fromJson(sb.toString(), Track.class);
                for (int i = 0; i < trackAI.getPieces().size(); i++) {

                    Piece piece = trackAI.getPiece(i);
                    piece.setIndex(i);

                    AIInfo info = piece.getAiInfo();
                    if (info.getCrashCount() == null) {

                        int[] crashCount = new int[track.getLanes().size()];

                        for (int c = 0; c < track.getLanes().size(); c++) {
                            crashCount[c] = 0;
                            if (info.getMinCrashAngle(c) < 90) {
                                crashCount[c]++;
                            }
                        }
                        info.setCrashCount(crashCount);
                    }
                    track.getPiece(i).setAiInfo(info);
                }
                logPrintln("Read track...");

            } catch (FileNotFoundException e) {

                e.printStackTrace();

            } catch (IOException e) {

                e.printStackTrace();

            } finally {

                try {
                    if (bf != null) {
                        bf.close();
                    }
                } catch (IOException e) {
                }
            }

            wroteTrack = false;

        } else {

            for (Piece p : track.getPieces()) {
                p.setAiInfo(new AIInfo(track.getLanes().size()));
            }

        }

    }

    private void writeTrackData(Track track) {
        if (track != null) {

            File file = new File(track.getId() + ".json");
            File dest = new File(track.getId() + ".json.bak");

            try {

                Gson gson = new GsonBuilder()
                        .serializeSpecialFloatingPointValues()
                        .create();
                String jsonString = gson.toJson(track);

                if (file.exists()) {
                    Files.copy(file.toPath(), dest.toPath());
                }

                PrintWriter pw = new PrintWriter(file);
                pw.write(jsonString);
                pw.close();

                wroteTrack = true;

                if (dest.exists()) {
                    Files.delete(dest.toPath());
                }
                logPrintln("Wrote track...");


            } catch (Exception e) {

                if (dest.exists()) {
                    try {
                        Files.copy(dest.toPath(), file.toPath());
                        logPrintln("Backup track restored...");
                    } catch (Exception e2) {
                        logPrintln("Backup track could not be restored...");
                    }
                }

                logPrintln(e.getMessage());
                e.printStackTrace();

            }
        } else {
            logPrintln("TRACK NULL !");
        }

    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
//        if (log) {
//            System.out.println("sent: " + msg.toJson());
//        }
    }

}
