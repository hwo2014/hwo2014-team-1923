package noobbot.model;

/**
 * Created by stefaanmoreels on 16/04/14.
 */
public class LapResult {

    Car car;
    LapTime result;

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public LapTime getResult() {
        return result;
    }

    public void setResult(LapTime result) {
        this.result = result;
    }
}
