package noobbot.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by stefaanmoreels on 21/04/14.
 */
public class Turbo {

/*
{
  "turboDurationMilliseconds": 500.0,
  "turboDurationTicks": 30,
  "turboFactor": 3.0
}
 */

    @SerializedName("turboDurationMilliseconds")
    double durationMilliseconds;
    @SerializedName("turboDurationTicks")
    long durationTicks;
    @SerializedName("turboFactor")
    double factor;


    public double getDurationMilliseconds() {
        return durationMilliseconds;
    }

    public void setDurationMilliseconds(double durationMilliseconds) {
        this.durationMilliseconds = durationMilliseconds;
    }

    public long getDurationTicks() {
        return durationTicks;
    }

    public void setDurationTicks(long durationTicks) {
        this.durationTicks = durationTicks;
    }

    public double getFactor() {
        return factor;
    }

    public void setFactor(double factor) {
        this.factor = factor;
    }
}
