package noobbot.model;

/**
 * Created by stefaanmoreels on 15/04/14.
 */
public class LaneSwitch {
    /*
    {
        "startLaneIndex": 0,
        "endLaneIndex": 0
      }
     */

    int startLaneIndex;
    int endLaneIndex;

    public int getStartLaneIndex() {
        return startLaneIndex;
    }

    public void setStartLaneIndex(int startLaneIndex) {
        this.startLaneIndex = startLaneIndex;
    }

    public int getEndLaneIndex() {
        return endLaneIndex;
    }

    public void setEndLaneIndex(int endLaneIndex) {
        this.endLaneIndex = endLaneIndex;
    }
}
