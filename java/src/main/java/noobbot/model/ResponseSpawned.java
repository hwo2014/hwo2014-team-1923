package noobbot.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by stefaanmoreels on 16/04/14.
 */
public class ResponseSpawned {

    /*
    {"msgType": "crash", "data": {
  "name": "Rosberg",
  "color": "blue"
}, "gameId": "OIUHGERJWEOI", "gameTick": 3}
     */

    String msgType;

    @SerializedName("data")
    Car car;

    String gameId;
    Long gameTick;

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public Long getGameTick() {
        return gameTick;
    }

    public void setGameTick(Long gameTick) {
        this.gameTick = gameTick;
    }
}
