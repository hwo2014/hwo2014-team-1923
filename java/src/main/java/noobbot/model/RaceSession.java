package noobbot.model;

/**
 * Created by stefaanmoreels on 22/04/14.
 */
public class RaceSession {

    /*
"raceSession": {
      "laps": 3,
      "maxLapTimeMs": 30000,
      "quickRace": true
    }


    "raceSession":{"durationMs":20000}
     */

    int laps = 0;
    Long maxLapTimeMs;
    boolean quickRace = true;
    Long durationMs;

    public RacePart getRacePart() {
        if (laps == 0) {
            return RacePart.qualification;
        } else {
            return RacePart.race;
        }
    }

    public int getLaps() {
        return laps;
    }

    public void setLaps(int laps) {
        this.laps = laps;
    }

    public Long getMaxLapTimeMs() {
        return maxLapTimeMs;
    }

    public void setMaxLapTimeMs(Long maxLapTimeMs) {
        this.maxLapTimeMs = maxLapTimeMs;
    }

    public boolean isQuickRace() {
        return quickRace;
    }

    public void setQuickRace(Boolean quickRace) {
        this.quickRace = quickRace;
    }

    public Long getDurationMs() {
        return durationMs;
    }

    public void setDurationMs(Long durationMs) {
        this.durationMs = durationMs;
    }
}

