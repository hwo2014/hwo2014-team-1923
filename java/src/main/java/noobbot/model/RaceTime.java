package noobbot.model;

/**
 * Created by stefaanmoreels on 16/04/14.
 */
public class RaceTime {
    /*
{
    "lap": 1,
    "ticks": 666,
    "millis": 6660
  }
     */

    int laps;
    long ticks;
    long millis;

    public int getLaps() {
        return laps;
    }

    public void setLaps(int laps) {
        this.laps = laps;
    }

    public long getTicks() {
        return ticks;
    }

    public void setTicks(long ticks) {
        this.ticks = ticks;
    }

    public long getMillis() {
        return millis;
    }

    public void setMillis(long millis) {
        this.millis = millis;
    }
}
