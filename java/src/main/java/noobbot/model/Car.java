package noobbot.model;

/**
 * Created by stefaanmoreels on 15/04/14.
 */
public class Car {
    /*
    {
      "name": "Schumacher",
      "color": "red"
    }
     */

    String name;
    String color;

    Turbo turbo;

    public String toString() {
        return new StringBuilder()
                .append(name)
                .append(" [").append(color).append("]")
                .toString();
    }

    public boolean equals(Car o) {
        return (name.equals(o.name) && color.equals(o.color));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean hasTurbo() {
        return (turbo != null);
    }

    public Turbo getTurbo() {
        return turbo;
    }

    public void setTurbo(Turbo turbo) {
        this.turbo = turbo;
    }
}
