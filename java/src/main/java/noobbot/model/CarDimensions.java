package noobbot.model;

/**
 * Created by stefaanmoreels on 22/04/14.
 */
public class CarDimensions {
    /*
{
          "length": 40.0,
          "width": 20.0,
          "guideFlagPosition": 10.0
        }
     */

    double length;
    double width;
    double guideFlagPosition;

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getGuideFlagPosition() {
        return guideFlagPosition;
    }

    public void setGuideFlagPosition(double guideFlagPosition) {
        this.guideFlagPosition = guideFlagPosition;
    }
}
