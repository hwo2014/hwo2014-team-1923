package noobbot.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by stefaanmoreels on 16/04/14.
 */
public class ResponseLapFinished {
    /*
    {"msgType": "lapFinished", "data": {
  "car": {
    "name": "Schumacher",
    "color": "red"
  },
  "lapTime": {
    "lap": 1,
    "ticks": 666,
    "millis": 6660
  },
  "raceTime": {
    "laps": 1,
    "ticks": 666,
    "millis": 6660
  },
  "ranking": {
    "overall": 1,
    "fastestLap": 1
  }
}, "gameId": "OIUHGERJWEOI", "gameTick": 300}
     */

    String msgType;

    @SerializedName("data")
    Lap lap;

    String gameId;
    long gameTick;

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public Lap getLap() {
        return lap;
    }

    public void setLap(Lap lap) {
        this.lap = lap;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public long getGameTick() {
        return gameTick;
    }

    public void setGameTick(long gameTick) {
        this.gameTick = gameTick;
    }
}
