package noobbot.model;

import java.util.List;

/**
 * Created by stefaanmoreels on 16/04/14.
 */
public class GameResults {
    /*
    {
  "results": [
    {
      "car": {
        "name": "Schumacher",
        "color": "red"
      },
      "result": {
        "laps": 3,
        "ticks": 9999,
        "millis": 45245
      }
    },
    {
      "car": {
        "name": "Rosberg",
        "color": "blue"
      },
      "result": {}
    }
  ],
  "bestLaps": [
    {
      "car": {
        "name": "Schumacher",
        "color": "red"
      },
      "result": {
        "lap": 2,
        "ticks": 3333,
        "millis": 20000
      }
    },
    {
      "car": {
        "name": "Rosberg",
        "color": "blue"
      },
      "result": {}
    }
  ]
}
     */


    List<GameResult> results;
    List<LapResult> bestLaps;

    public List<GameResult> getResults() {
        return results;
    }

    public void setResults(List<GameResult> results) {
        this.results = results;
    }

    public List<LapResult> getBestLaps() {
        return bestLaps;
    }

    public void setBestLaps(List<LapResult> bestLaps) {
        this.bestLaps = bestLaps;
    }
}
