package noobbot.model;

/**
 * Created by stefaanmoreels on 21/04/14.
 */
public class Crash {

    String trackId;
    int pieceIndex;
    int laneIndex;
    double inPieceDistance;
    double carAngle;
    double speed;

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public int getPieceIndex() {
        return pieceIndex;
    }

    public void setPieceIndex(int pieceIndex) {
        this.pieceIndex = pieceIndex;
    }

    public double getInPieceDistance() {
        return inPieceDistance;
    }

    public void setInPieceDistance(double inPieceDistance) {
        this.inPieceDistance = inPieceDistance;
    }

    public double getCarAngle() {
        return carAngle;
    }

    public void setCarAngle(double carAngle) {
        this.carAngle = carAngle;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public int getLaneIndex() {
        return laneIndex;
    }

    public void setLaneIndex(int laneIndex) {
        this.laneIndex = laneIndex;
    }
}
