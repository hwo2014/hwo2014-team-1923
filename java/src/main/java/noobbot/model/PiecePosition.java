package noobbot.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by stefaanmoreels on 15/04/14.
 */
public class PiecePosition {
    /*
    {
      "pieceIndex": 0,
      "inPieceDistance": 0.0,
      "lane": {
        "startLaneIndex": 0,
        "endLaneIndex": 0
      },
      "lap": 0
    }
     */

    int pieceIndex;

    double inPieceDistance;

    @SerializedName("lane")
    LaneSwitch laneSwitch;

    int lap;

    public String toString() {
        return new StringBuilder()
                .append("PiecePosition: ")
                .append(pieceIndex)
                .append(" [")
                .append(String.format("%.2f", inPieceDistance))
                .append("] ")
                .append(lap)
                .append(" ON " + laneSwitch.startLaneIndex + " -> " + laneSwitch.endLaneIndex)
                .toString();
    }

    public int getPieceIndex() {
        return pieceIndex;
    }

    public void setPieceIndex(int pieceIndex) {
        this.pieceIndex = pieceIndex;
    }

    public double getInPieceDistance() {
        return inPieceDistance;
    }

    public void setInPieceDistance(double inPieceDistance) {
        this.inPieceDistance = inPieceDistance;
    }

    public LaneSwitch getLaneSwitch() {
        return laneSwitch;
    }

    public void setLaneSwitch(LaneSwitch laneSwitch) {
        this.laneSwitch = laneSwitch;
    }

    public int getLap() {
        return lap;
    }

    public void setLap(int lap) {
        this.lap = lap;
    }
}
