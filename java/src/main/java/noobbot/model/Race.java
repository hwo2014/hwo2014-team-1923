package noobbot.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stefaanmoreels on 15/04/14.
 */
public class Race {

    Track track;

    @SerializedName("cars")
    List<CarSpecification> carSpecifications = new ArrayList<CarSpecification>();

    RaceSession raceSession;

    public String toString() {
        return new StringBuilder()
                .append(track)
                .toString();
    }

    public Track getTrack() {
        return track;
    }

    public void setTrack(Track track) {
        this.track = track;
    }

    public List<CarSpecification> getCarSpecifications() {
        return carSpecifications;
    }

    public void setCarSpecifications(List<CarSpecification> carSpecifications) {
        this.carSpecifications = carSpecifications;
    }

    public RaceSession getRaceSession() {
        return raceSession;
    }

    public void setRaceSession(RaceSession raceSession) {
        this.raceSession = raceSession;
    }
}
