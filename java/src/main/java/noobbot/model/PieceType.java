package noobbot.model;

/**
 * Created by stefaanmoreels on 15/04/14.
 */
public enum PieceType {
    straight, curveLeft, curveRight;
}
