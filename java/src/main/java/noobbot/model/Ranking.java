package noobbot.model;

/**
 * Created by stefaanmoreels on 16/04/14.
 */
public class Ranking {
    /*
    {
    "overall": 1,
    "fastestLap": 1
  }
     */

    int overall;
    int fastestLap;

    public int getOverall() {
        return overall;
    }

    public void setOverall(int overall) {
        this.overall = overall;
    }

    public int getFastestLap() {
        return fastestLap;
    }

    public void setFastestLap(int fastestLap) {
        this.fastestLap = fastestLap;
    }
}
