package noobbot.model;

import com.google.gson.annotations.SerializedName;

import noobbot.Main;

/**
 * Created by stefaanmoreels on 15/04/14.
 */
public class Piece {

/*
{
          "length": 100.0
        },
        {
          "length": 100.0,
          "switch": true
        },
        {
          "radius": 200,
          "angle": 22.5
        }
 */

    double length = -1;
    @SerializedName("switch")
    boolean laneSwitch = false;
    double radius = -1;
    double angle = 0;

    AIInfo aiInfo ;
    int index = -1;


    public String toString() {
        return toString(0);
    }
    public String toString(double laneDelta) {
        StringBuilder sb = new StringBuilder();
        PieceType pt = getType();
        switch (pt) {
            case straight:
                sb.append(" S: " + String.format("%5.1f", length) + "        ");
                break;
            case curveLeft:
                sb.append(" L: " + String.format("%5.1f", getLength(laneDelta)) );
                sb.append(" @" + String.format("%6.2f", Math.abs(angle)));
                break;
            case curveRight:
                sb.append(" R: " + String.format("%5.1f", getLength(laneDelta)) );
                sb.append(" @" + String.format("%6.2f", angle));
                break;
        }
        if (laneSwitch) {
            sb.append(" (X)");
        } else {
            sb.append(" (|)");
        }

        return sb.toString();
    }

    public PieceType getType() {
        if (length > 0) {
            return PieceType.straight;
        } else if (angle < 0) {
            return PieceType.curveLeft;
        } else {
            return PieceType.curveRight;
        }
    }

    public boolean equals(Piece o) {
        if (getType() != o.getType()) {
            return false;
        } else {
            switch (getType()) {
                case straight:
                    return (length == o.length);
                default:
                    return (angle == o.angle && radius == o.radius);
            }
        }
    }

    public double getLength(double laneDelta) {


        if (getType().equals(PieceType.straight)) {
            return length;
        }
        double rad = Math.abs(angle) * Math.PI / 180;
        if (getType().equals(PieceType.curveRight)) {
            return (rad * (radius - laneDelta));
        } else {
            return (rad * (radius + laneDelta));
        }
    }

    public double getLength() {
        return getLength(0);
    }

    public void setLength(double length) {
        this.length = length;
    }

    public boolean isLaneSwitch() {
        return laneSwitch;
    }

    public void setLaneSwitch(boolean laneSwitch) {
        this.laneSwitch = laneSwitch;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public AIInfo getAiInfo() {
        return aiInfo;
    }

    public void setAiInfo(AIInfo aiInfo) {
        this.aiInfo = aiInfo;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Piece getNext() {
        return Main.track.getPiece(index + 1);
    };
    public Piece getPrev() {
        return Main.track.getPiece(index - 1);
    };

}
