package noobbot.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by stefaanmoreels on 16/04/14.
 */
public class ResponseYourCar {

    /*
    {"msgType":"yourCar","data":{"name":"Albatroz","color":"red"},"gameId":"d958a453-592b-4eb0-8a77-535762e2262d"}
     */

    String msgType;

    @SerializedName("data")
    Car car;

    String gameId;

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }
}
