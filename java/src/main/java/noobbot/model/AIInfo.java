package noobbot.model;

/**
 * Created by stefaanmoreels on 21/04/14.
 */
public class AIInfo {

    public static int MAXLANES = 4;

    //List<Crash> crashes = new ArrayList<Crash>();

    int[] crashCount = new int[]{0, 0, 0, 0};
    double[] minCrashAngle;
    double[] minCrashSpeed;
    //double[] avgSpeed;
    double[] maxSpeed;
    double[] avgCrashDistance;

    public AIInfo(int laneNum) {

        MAXLANES = laneNum;
        minCrashAngle = new double[MAXLANES];
        minCrashSpeed = new double[MAXLANES];

        maxSpeed = new double[MAXLANES];
        crashCount = new int[MAXLANES];
        avgCrashDistance = new double[MAXLANES];

        for (int i = 0; i < MAXLANES; i++) {
            minCrashAngle[i] = 90;
            minCrashSpeed[i] = 100;
            maxSpeed[i] = 0;
            avgCrashDistance[i] = -1;
            //avgSpeed[i] = 0;
            crashCount[i] = 0;
        }

    }

    public String getLaneInfo(int laneIndex) {
        return new StringBuilder()
                .append(" AI: ")
                .append(String.format("%4.2f", minCrashSpeed[laneIndex]))
                .append(" @ ")
                .append(String.format("%4.2f", minCrashAngle[laneIndex]))
                .append(" max: ")
                .append(String.format("%4.2f", maxSpeed[laneIndex]))
                .append(" ")
                .toString();
    }

    public String toString() {
        return new StringBuilder()
                .append(" AI: ")
                .append(minCrashSpeed)
                .append(" @ ")
                .append(minCrashAngle)
                .append(" max: ")
                .append(maxSpeed)
                .append(" ")
                .toString();
    }

    private boolean checkLane(int laneIndex) {
        return (laneIndex >= 0 && laneIndex < MAXLANES);
    }

    public double getMaxSpeed(int laneIndex) {

        if (!checkLane(laneIndex)) {
            return 0;
        }

        return maxSpeed[laneIndex];
    }

    public void setMaxSpeed(int laneIndex, double maxSpeedIn) {
        if (checkLane(laneIndex)) {
            this.maxSpeed[laneIndex] = maxSpeedIn;
        }
    }

//    public double getAvgSpeed(int laneIndex) {
//
//        if (!checkLane(laneIndex)) {
//            return 0;
//        }
//
//        return avgSpeed[laneIndex];
//    }

//    public void setAvgSpeed(int laneIndex, double maxSpeedIn) {
//        if (checkLane(laneIndex)) {
//            this.avgSpeed[laneIndex] = maxSpeedIn;
//        }
//    }

//    public List<Crash> getCrashes() {
//        return crashes;
//    }

//    public void setCrashes(List<Crash> crashes) {
//        this.crashes = crashes;
//    }

    public double getMinCrashAngle(int laneIndex) {
        if (!checkLane(laneIndex)) {
            return 90;
        }

        return minCrashAngle[laneIndex];
    }

    public void setMinCrashAngle(int laneIndex, double minCrashAngle) {
        if (checkLane(laneIndex)) {
            this.minCrashAngle[laneIndex] = minCrashAngle;
        }
    }

    public double getMinCrashSpeed(int laneIndex) {

        if (!checkLane(laneIndex)) {
            return 100;
        }
        return minCrashSpeed[laneIndex];
    }

    public void setMinCrashSpeed(int laneIndex, double minCrashSpeed) {
        if (checkLane(laneIndex)) {
            this.minCrashSpeed[laneIndex] = minCrashSpeed;
        }
    }

    public int getCrashCount(int laneIndex) {
        if (!checkLane(laneIndex)) {
            return 0;
        }
        return crashCount[laneIndex];
    }

    public void addCrashOnLane(int laneIndex) {
        if (checkLane(laneIndex)) {
            crashCount[laneIndex]++;
        }
    }

    public int[] getCrashCount() {
        return crashCount;
    }

    public boolean isCrashPiece() {
        for (int crashCnt : crashCount) {
            if (crashCnt > 0) {
                return true;
            }
        }
        return false;
    }

    public boolean isCrashPiece(int laneIndex) {
        return (crashCount[laneIndex] > 0);
    }

    public void setCrashCount(int[] crashCount) {
        this.crashCount = crashCount;
    }

    public double getAvgCrashDistance(int laneIndex) {
        if (this.avgCrashDistance == null) {
            avgCrashDistance = new double[MAXLANES];
            for (int i = 0; i < MAXLANES; i++) {
                this.avgCrashDistance[i] = -1;
            }

        }
        return avgCrashDistance[laneIndex];
    }

    public void setAvgCrashDistance(int laneIndex, double avgCrashDistance) {
        if (this.avgCrashDistance == null) {
            this.avgCrashDistance = new double[MAXLANES];
            for (int i = 0; i < MAXLANES; i++) {
                this.avgCrashDistance[i] = -1;
            }

        }
        this.avgCrashDistance[laneIndex] = avgCrashDistance;
    }

    public double[] getAvgCrashDistance() {
        return avgCrashDistance;
    }

    public void setAvgCrashDistance(double[] avgCrashDistance) {
        this.avgCrashDistance = avgCrashDistance;
    }
}

