package noobbot.model;

/**
 * Created by stefaanmoreels on 15/04/14.
 */
public class Lane {
/*
    {
        "distanceFromCenter": -20,
            "index": 0
    }
*/

    double distanceFromCenter;
    int index;

    public String toString() {
        return new StringBuilder()
                .append("Lane ")
                .append(index)
                .append(" [")
                .append(distanceFromCenter)
                .append("]")
                .toString();


    }

    public double getDistanceFromCenter() {
        return distanceFromCenter;
    }

    public void setDistanceFromCenter(double distanceFromCenter) {
        this.distanceFromCenter = distanceFromCenter;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
