package noobbot.model;

/**
 * Created by stefaanmoreels on 22/04/14.
 */
public enum RacePart {
    qualification, race
}
