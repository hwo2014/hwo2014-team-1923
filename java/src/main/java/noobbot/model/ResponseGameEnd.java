package noobbot.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by stefaanmoreels on 16/04/14.
 */
public class ResponseGameEnd {

    /*
{"msgType": "gameEnd", "data": {
  "results": [
    {
      "car": {
        "name": "Schumacher",
        "color": "red"
      },
      "result": {
        "laps": 3,
        "ticks": 9999,
        "millis": 45245
      }
    },
    {
      "car": {
        "name": "Rosberg",
        "color": "blue"
      },
      "result": {}
    }
  ],
  "bestLaps": [
    {
      "car": {
        "name": "Schumacher",
        "color": "red"
      },
      "result": {
        "lap": 2,
        "ticks": 3333,
        "millis": 20000
      }
    },
    {
      "car": {
        "name": "Rosberg",
        "color": "blue"
      },
      "result": {}
    }
  ]
}}
     */

    String msgType;

    @SerializedName("data")
    GameResults gameResults;

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public GameResults getGameResults() {
        return gameResults;
    }

    public void setGameResults(GameResults gameResults) {
        this.gameResults = gameResults;
    }
}
