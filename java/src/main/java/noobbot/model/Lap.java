package noobbot.model;

/**
 * Created by stefaanmoreels on 16/04/14.
 */
public class Lap {
    /*
    {
  "car": {
    "name": "Schumacher",
    "color": "red"
  },
  "lapTime": {
    "lap": 1,
    "ticks": 666,
    "millis": 6660
  },
  "raceTime": {
    "laps": 1,
    "ticks": 666,
    "millis": 6660
  },
  "ranking": {
    "overall": 1,
    "fastestLap": 1
  }
}
     */

    Car car;
    LapTime lapTime;
    RaceTime raceTime;
    Ranking ranking;

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public LapTime getLapTime() {
        return lapTime;
    }

    public void setLapTime(LapTime lapTime) {
        this.lapTime = lapTime;
    }

    public RaceTime getRaceTime() {
        return raceTime;
    }

    public void setRaceTime(RaceTime raceTime) {
        this.raceTime = raceTime;
    }

    public Ranking getRanking() {
        return ranking;
    }

    public void setRanking(Ranking ranking) {
        this.ranking = ranking;
    }
}
