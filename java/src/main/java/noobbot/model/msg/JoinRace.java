package noobbot.model.msg;

import com.google.gson.annotations.SerializedName;

import noobbot.model.Bot;

/**
 * Created by stefaanmoreels on 16/04/14.
 */
public class JoinRace extends SendMsg {
    /*
    {"msgType": "joinRace", "data": {
  "botId": {
    "name": "keke",
    "key": "IVMNERKWEW"
  },
  "trackName": "hockenheimring",
  "password": "schumi4ever",
  "carCount": 3
}}
     */

    @SerializedName("botId")
    Bot bot;

    String trackName;

    String password;
    Integer carCount;

    public JoinRace(String name, String key) {
        this(new Bot(name,key));
    }
    public JoinRace(Bot bot) {
        this.bot = bot;
    }

    public JoinRace(String name, String key, String trackName) {
        this(new Bot(name,key), trackName);
    }
    public JoinRace(Bot bot, String trackName) {
        this.bot = bot;
        this.trackName = trackName;
    }

    public JoinRace(String name, String key, String trackName, Integer carCount) {
        this(new Bot(name,key), trackName, carCount);
    }
    public JoinRace(Bot bot, String trackName, Integer carCount) {
        this.bot = bot;
        this.trackName = trackName;
        this.carCount = carCount;
    }


    public JoinRace(String name, String key, String trackName, String password) {
        this(new Bot(name,key), trackName, password);
    }
    public JoinRace(Bot bot, String trackName, String password) {
        this.bot = bot;
        this.trackName = trackName;
        this.password = password;
    }

    public JoinRace(String name, String key, String trackName, String password, Integer carCount) {
        this(new Bot(name,key), trackName, password, carCount);
    }
    public JoinRace(Bot bot, String trackName, String password, Integer carCount) {
        this.bot = bot;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}
