package noobbot.model.msg;

/**
 * Created by stefaanmoreels on 22/04/14.
 */
public class TurboRequest extends SendMsg {

    private String value;

    public TurboRequest(String value) {
        this.value = value;
    }
    public TurboRequest(String value, Long gameTick) {
        this(value);
        this.gameTick = gameTick;

    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "turbo";
    }
}
