package noobbot.model.msg;

/**
 * Created by stefaanmoreels on 15/04/14.
 */
public class Join extends SendMsg {
    public final String name;
    public final String key;

    public Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}
