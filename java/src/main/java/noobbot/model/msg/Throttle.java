package noobbot.model.msg;

/**
 * Created by stefaanmoreels on 15/04/14.
 */
public class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }
    public Throttle(double value, Long gameTick) {
        this(value);
        this.gameTick = gameTick;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}
