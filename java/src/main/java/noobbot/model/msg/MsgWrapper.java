package noobbot.model.msg;

/**
 * Created by stefaanmoreels on 15/04/14.
 */
public class MsgWrapper {
    public final String msgType;
    public final Object data;
    public final Long gameTick;

    MsgWrapper(final String msgType, final Object data, final Long gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameTick = gameTick;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData(), sendMsg.gameTick);
    }
}

