package noobbot.model.msg;

import noobbot.model.SwitchLaneType;

/**
 * Created by stefaanmoreels on 15/04/14.
 */
public class SwitchLane extends SendMsg {
    private SwitchLaneType value;

    public SwitchLane(SwitchLaneType value) {
        this.value = value;
    }

    public SwitchLane(SwitchLaneType value, Long gameTick) {
        this(value);
        this.gameTick = gameTick;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}
