package noobbot.model.msg;

import com.google.gson.Gson;

/**
 * Created by stefaanmoreels on 15/04/14.
 */
public abstract class SendMsg {

    protected Long gameTick;

    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }
    protected abstract String msgType();
}
