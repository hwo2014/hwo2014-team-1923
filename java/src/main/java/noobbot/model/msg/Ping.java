package noobbot.model.msg;

/**
 * Created by stefaanmoreels on 15/04/14.
 */
public class Ping extends SendMsg {

    @Override
    protected String msgType() {
        return "ping";
    }
}

