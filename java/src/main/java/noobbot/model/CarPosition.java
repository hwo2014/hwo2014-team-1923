package noobbot.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by stefaanmoreels on 15/04/14.
 */
public class CarPosition {
    /*
    {
    "id": {
      "name": "Schumacher",
      "color": "red"
    },
    "angle": 0.0,
    "piecePosition": {
      "pieceIndex": 0,
      "inPieceDistance": 0.0,
      "lane": {
        "startLaneIndex": 0,
        "endLaneIndex": 0
      },
      "lap": 0
    }
  }
     */



    @SerializedName("id")
    Car car;
    Double angle;
    PiecePosition piecePosition;

    @Override
    public String toString() {
        return new StringBuilder("Car: ")
                .append( car )
                .append(" : @")
                .append(String.format("%.2f", angle))
                .append(" -> " + piecePosition)
                .toString();
    }


    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Double getAngle() {
        return angle;
    }

    public void setAngle(Double angle) {
        this.angle = angle;
    }

    public PiecePosition getPiecePosition() {
        return piecePosition;
    }

    public void setPiecePosition(PiecePosition piecePosition) {
        this.piecePosition = piecePosition;
    }



}
