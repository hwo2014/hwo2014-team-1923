package noobbot.model;

/**
 * Created by stefaanmoreels on 22/04/14.
 */
public class ResponseTournamentEnd {

    /*
    {"msgType": "tournamentEnd", "data": null}
     */

    String msgType;
    Object data;

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
