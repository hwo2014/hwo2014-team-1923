package noobbot.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by stefaanmoreels on 22/04/14.
 */
public class CarSpecification {

    /*
{
        "id": {
          "name": "Rosberg",
          "color": "blue"
        },
        "dimensions": {
          "length": 40.0,
          "width": 20.0,
          "guideFlagPosition": 10.0
        }
      }
     */

    @SerializedName("id")
    Car car;

    CarDimensions dimensions;

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public CarDimensions getDimensions() {
        return dimensions;
    }

    public void setDimensions(CarDimensions dimensions) {
        this.dimensions = dimensions;
    }
}
