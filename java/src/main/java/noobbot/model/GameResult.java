package noobbot.model;

/**
 * Created by stefaanmoreels on 16/04/14.
 */
public class GameResult {
    /*
    {
      "car": {
        "name": "Schumacher",
        "color": "red"
      },
      "result": {
        "laps": 3,
        "ticks": 9999,
        "millis": 45245
      }
    }
     */

    Car car;
    RaceTime result;

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public RaceTime getResult() {
        return result;
    }

    public void setResult(RaceTime result) {
        this.result = result;
    }
}
