package noobbot.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by stefaanmoreels on 15/04/14.
 */
public class Track {

    /*
    {
      "id": "indianapolis",
      "name": "Indianapolis",
      "pieces": [
        {
          "length": 100.0
        },
        {
          "length": 100.0,
          "switch": true
        },
        {
          "radius": 200,
          "angle": 22.5
        }
      ],
      "lanes": [
        {
          "distanceFromCenter": -20,
          "index": 0
        },
        {
          "distanceFromCenter": 0,
          "index": 1
        },
        {
          "distanceFromCenter": 20,
          "index": 2
        }
      ],
      "startingPoint": {
        "position": {
          "x": -340.0,
          "y": -96.0
        },
        "angle": 90.0
      }
    }
     */



    String id;
    String name;
    List<Piece> pieces;
    List<Lane> lanes;

    double avgAccelleration = 0;
    double avgBraking = 0;

    public String toString() {
        StringBuilder sb = new StringBuilder()
                .append("Track: ")
                .append(name)
                .append(" Pieces:\n");

        for (int i = 0 ; i < pieces.size() ; i++) {
            sb.append(String.format("%2d", i))
                    .append(": ").append(pieces.get(i)).append("\n");
        }

        sb.append(" Lanes: ").append(lanes);

        return sb.toString();
    }


    public Piece getPiece(int index) {

        while (index < 0) {
            index += pieces.size();
        }

        return pieces.get( index % pieces.size() );
    }

    public double[] getDistanceUntilNextSwitch(int startIndex) {
        double[] retVal = new double[lanes.size()];
        Piece piece = getPiece(startIndex);
        while (!piece.isLaneSwitch()) {
            for (int i = 0; i < lanes.size(); i++) {
                retVal[i] += piece.getLength(lanes.get(i).distanceFromCenter);
            }
            piece = getPiece(startIndex++);
        }
        return retVal;
    }

    public double[] getTicksUntilNextSwitch(int startIndex) {

        double[] retVal = new double[lanes.size()];

        int nextSwitch = getNextSwitch(startIndex + 1);

        for (int i = 0 ; i < lanes.size(); i++) {
            retVal[i] = getTicksBetween(startIndex, nextSwitch, i);
        }
        return retVal;
    }


    public double[] getTicksBetween(int startIndex, int stopIndex) {

        double[] retVal = new double[lanes.size()];
        for (int i = 0 ; i < lanes.size(); i++) {
            retVal[i] = getTicksBetween(startIndex, stopIndex, i);
        }
        return retVal;
    }

    public double getTicksBetween(int startIndex, int stopIndex, int laneIndex) {

        double retVal = 0;

        stopIndex = stopIndex % pieces.size();
        double laneDistance = lanes.get(laneIndex).distanceFromCenter;

        for (int i = startIndex; i < stopIndex; i = (i+1) % pieces.size()) {

            Piece piece = getPiece(i);
            double length = piece.getLength(laneDistance);

            if (piece.aiInfo.getCrashCount(laneIndex) > 0) {
                retVal += length / piece.aiInfo.getMinCrashSpeed(laneIndex);
            } else if (piece.aiInfo.getMaxSpeed(laneIndex) > 0 ) {
                retVal += length / piece.aiInfo.getMaxSpeed(laneIndex);
            }

        }

        return retVal;

    }

    public Piece getNextCrashPiece(int index, int laneIndex) {
        index = (index + pieces.size()) % pieces.size();
        int count = index;

        Piece next = getPiece(count); // 5
        count = (count + 1) % pieces.size();

        if (next.getAiInfo().isCrashPiece(laneIndex)) {
            return next;
        }

        while ( !next.getAiInfo().isCrashPiece(laneIndex) && index != count ) {
            next = getPiece(count);
            count = (count + 1) % pieces.size();
        }
        return next;

    }



    public Piece getNextCrashPiece(int index) {
        int start = index;

        index = (index + pieces.size()) % pieces.size();
        Piece next = getPiece(index++);

        while ( !next.getAiInfo().isCrashPiece() && index != start ) {
            next = getPiece(index++);
        }
        return next;

    }

    public int getNextSwitch(int index) {
        int start = index % pieces.size();
        index = (index+1) % pieces.size();

        while ( index != start ) {
            Piece next = getPiece(index);
            if (next.isLaneSwitch()) {
                break;
            }
            index = index + 1;
        }
        return index;
    }


    public boolean isStraight(int index) {
        return getPiece(index).getType().equals(PieceType.straight);
    }
    public boolean isStraightLane(int index) {

        return isStraight(index) &&
                isStraight(index + 1) &&
                isStraight(index + 2) &&
                isStraight(index + 3);
    }

    public PieceType getCurveTypeUntilNextSwitch(int index) {

        int numLeft = 0;
        int numRight = 0;

        int nextSwitch = getNextSwitch(index);
        for (int i = index + 1 ; i < nextSwitch; i++ ) {
            switch (getPiece(i).getType()) {
                case curveLeft:
                    numLeft++;
                    break;
                case curveRight:
                    numRight++;
                    break;
            }
        }

        if (numLeft == 0 && numRight == 0) {
            return PieceType.straight;
        } else if (numLeft >= numRight) {
            return PieceType.curveLeft;
        } else {
            return PieceType.curveRight;
        }
    }


    static Map<Integer, Boolean> nextCurves = new HashMap<Integer, Boolean>();
    public boolean nextCurve(int index) {

        Boolean nCurvesVal = nextCurves.get(index);
        if (nCurvesVal != null) {
            return nCurvesVal;
        }

        for (int i = index + 1 ; i < index + 4 ; i++) {

            Piece p1 = getPiece(i);
            if (p1.getType().equals(PieceType.straight)) {
                nextCurves.put(index, false);
                return false;
            }
            Piece p2 = getPiece(i + 1);
            if (p2.getType().equals(PieceType.straight)) {
                nextCurves.put(index, false);
                return false;
            }

            if (p1.getAngle() != p2.getAngle()) {
                nextCurves.put(index, false);
                return false;
            }


        }
        nextCurves.put(index, true);
        return true;
    }

    static Map<Integer, Boolean> inCurves = new HashMap<Integer, Boolean>();
    public boolean inCurve(int index) {

        Boolean nCurvesVal = inCurves.get(index);
        if (nCurvesVal != null) {
            return nCurvesVal;
        }

        for (int i = index + 0 ; i < index + 3 ; i++) {
            Piece p1 = getPiece(i);
            if (p1.getType().equals(PieceType.straight)) {
                inCurves.put(index, false);
                return false;
            }
            Piece p2 = getPiece(i + 1);
            if (p2.getType().equals(PieceType.straight)) {
                inCurves.put(index, false);
                return false;
            }

            if (p1.getAngle() != p2.getAngle()) {
                inCurves.put(index, false);
                return false;
            }

        }
        inCurves.put(index, true);
        return true;
    }

    static Map<Integer, Boolean> outCurves = new HashMap<Integer, Boolean>();
    public boolean outCurve(int index) {

        Boolean nCurvesVal = outCurves.get(index);
        if (nCurvesVal != null) {
            return nCurvesVal;
        }

        for (int i = index - 2 ; i < index ; i++) {
            Piece p1 = getPiece(i);
            if (p1.getType().equals(PieceType.straight)) {
                outCurves.put(index, false);
                return false;
            }
            Piece p2 = getPiece(i + 1);
            if (p2.getType().equals(PieceType.straight)) {
                outCurves.put(index, false);
                return false;
            }

            if (p1.getAngle() != p2.getAngle()) {
                outCurves.put(index, false);
                return false;
            }

        }
        outCurves.put(index, true);
        return true;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Piece> getPieces() {
        return pieces;
    }

    public void setPieces(List<Piece> pieces) {
        this.pieces = pieces;
    }

    public List<Lane> getLanes() {
        return lanes;
    }

    public void setLanes(List<Lane> lanes) {
        this.lanes = lanes;
    }

    public double getAvgAccelleration() {
        return avgAccelleration;
    }

    public void setAvgAccelleration(double avgAccelleration) {
        this.avgAccelleration = avgAccelleration;
    }

    public double getAvgBraking() {
        return avgBraking;
    }

    public void setAvgBraking(double avgBraking) {
        this.avgBraking = avgBraking;
    }
}
