package noobbot.model;

/**
 * Created by stefaanmoreels on 16/04/14.
 */
public class LapTime {
    /*
{
    "lap": 1,
    "ticks": 666,
    "millis": 6660
  }
     */

    int lap;
    long ticks;
    long millis;

    public int getLap() {
        return lap;
    }

    public void setLap(int lap) {
        this.lap = lap;
    }

    public long getTicks() {
        return ticks;
    }

    public void setTicks(long ticks) {
        this.ticks = ticks;
    }

    public long getMillis() {
        return millis;
    }

    public void setMillis(long millis) {
        this.millis = millis;
    }
}
