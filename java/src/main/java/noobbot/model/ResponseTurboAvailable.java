package noobbot.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by stefaanmoreels on 21/04/14.
 */
public class ResponseTurboAvailable {

    /*

    {"msgType": "turboAvailable", "data": {
  "turboDurationMilliseconds": 500.0,
  "turboDurationTicks": 30,
  "turboFactor": 3.0
}}

     */

    String msgType;

    @SerializedName("data")
    Turbo turbo;

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public Turbo getTurbo() {
        return turbo;
    }

    public void setTurbo(Turbo turbo) {
        this.turbo = turbo;
    }
}
