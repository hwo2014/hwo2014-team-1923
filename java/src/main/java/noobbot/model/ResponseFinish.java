package noobbot.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by stefaanmoreels on 16/04/14.
 */
public class ResponseFinish {

    /*
    {"msgType": "finish", "data": {
  "name": "Schumacher",
  "color": "red"
}, "gameId": "OIUHGERJWEOI", "gameTick": 2345}
     */

    String msgType;

    @SerializedName("data")
    Car car;

    String gameId;
    long gameTick;


    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public long getGameTick() {
        return gameTick;
    }

    public void setGameTick(long gameTick) {
        this.gameTick = gameTick;
    }
}
