package noobbot.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by stefaanmoreels on 15/04/14.
 */
public class ResponseCarPositions {

    String msgType;

    @SerializedName("data")
    List<CarPosition> carPositions;

    String gameId;
    long gameTick;

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public List<CarPosition> getCarPositions() {
        return carPositions;
    }

    public void setCarPositions(List<CarPosition> carPositions) {
        this.carPositions = carPositions;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public long getGameTick() {
        return gameTick;
    }

    public void setGameTick(long gameTick) {
        this.gameTick = gameTick;
    }

    public String toString() {
        return new StringBuilder()
                .append("CarPositions: " + carPositions)
                .toString();

    }
}
